+++
title = 'Kobo Elipsa 2E'
+++

I bought this device in 2024 to have at least some e-reader with >= 10" display
for casual reading of books and various technical documents
until I find or perhaps create my dream e-reader capable of running full-featured web-browser
without any subscription cloud bullshit or home-calling to its vendor or CCP.

_Kobo Elipsa 2E_ can be used without any accounts or internet
so it's fitting a role of such temporary device for me well.

Of course Kobo does not make it super simple and the following repo documents and automates my setup:

## [Kobo Elipsa 2E setup](https://codeberg.org/Uli/kobo-elipsa-2e-setup)

{{% notice title="Excerpt" %}}
{{% mdextract resource="README.txt" heading="Offline recovery and setup" recursive=true level=4 %}}
{{% mdextract resource="README.txt" heading="`installer.py`" recursive=false level=4 %}}

...
{{% /notice %}}
