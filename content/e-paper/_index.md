+++
title = "E-paper"
type = "chapter"
+++

I ❤️ E-paper displays. When viewed just with ambient light (i.e. without any frontlight) they look absolutely magical.

I do not care about their low power consumption much.
For casual book reading I would still prefer an e-reader over a classical tablet even if it had the same battery life.

I strongly believe they also belong to fixed installations e.g. control panels where low display latency is not required
like in home automation or as secondary desktop displays for reading documentation/schematics.
